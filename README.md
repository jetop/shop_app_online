# Shop App (online)

A new project to learn Firebase on Flutter.

## What is this?

Welcome to Shop App, an app thought to learn Firebase on Flutter's basics. Is is being made by Edoardo and Matteo, and it will be part of the workshop about app development at JUNITEC. In case you have any questions, please feel free to get in touch at [e.debenedetti@jetop.com](mailto:e.debenedetti@jetop.com) or [m.demartis@jetop.com](mailto:m.demartis@jetop.com).

This app is thought to be used online, with Firebase as backend. The part without the backend (with fake data), take a look at [this](https://bitbucket.org/jetop/shop_app_offline) repository.

The app has some very basic features, such as a login screen, a list of the available products in the shop, and some info about them.