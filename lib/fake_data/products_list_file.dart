import '../support_classes/product_data.dart';

/// Data with a fake list of products
class ProductsListData {

  /// Contains a fake list of products, in form of [ProductData]
  static const List<ProductData> data = [
    ProductData('Pizza', 5.3, 6, 'pizza'),
    ProductData('Pasta', 2.4, 100, 'pasta')
  ];
}