class ProductData {
  
  final String name;
  final double price;
  final int quantity;
  final String id;

  const ProductData(this.name, this.price, this.quantity, this.id);

}