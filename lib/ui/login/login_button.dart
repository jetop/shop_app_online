import 'package:flutter/material.dart';
import '../../firebase/authenticator.dart';
import '../product_list/product_list_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginButton extends StatelessWidget {
  final loginText = 'Login!';
  final Authenticator authenticator = new Authenticator();

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(loginText),
      onPressed: () async {
        try {
          FirebaseUser user = await authenticator.login();

          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) =>
                  ProductListScreen("Hello " + user.displayName)));
        }
        catch(error){
          showDialog(
              context: context,
              builder: (BuildContext context){
                return AlertDialog(
                  title: new Text('Auth error'),
                  content: new Text('Authentication error!')
                );
              }
          );
        }
      },
    );
  }
}
