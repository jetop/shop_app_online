import 'package:flutter/material.dart';
import '../../support_classes/product_data.dart';
import 'product_info.dart';

class ProductPage extends StatelessWidget {

  final ProductData data;

  const ProductPage(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Info about " + data.name),
      ),
      body: ProductInfo(data),
    );
  }
  
}