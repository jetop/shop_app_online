import 'package:flutter/material.dart';
import '../../support_classes/product_data.dart';

class ProductInfo extends StatelessWidget {
  final ProductData data;

  const ProductInfo(this.data);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InfoElement(data.name, "Name", Icons.label),
        InfoElement(data.id, "Id", Icons.info),
        InfoElement(data.price.toString(), "Price", Icons.euro_symbol),
        InfoElement(data.quantity.toString(), "Quantity", Icons.filter_9_plus)
      ],
    );
  }
  
}

class InfoElement extends StatelessWidget {
  final String data;
  final String name;
  final IconData icon;

  const InfoElement(this.data, this.name, this.icon);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon),
      title: Row(
        children: <Widget>[
          Text(
            name + ": ",
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
          Text(data),
        ],
      ),
    );
  }
}
