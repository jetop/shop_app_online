import 'package:flutter/material.dart';
import '../../support_classes/product_data.dart';
import '../product_page/product_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class ProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('product').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            return new ListView(
              children: snapshot.data.documents.map((DocumentSnapshot document) {
                ProductData d = ProductData(document['name'], document['price'], document['quantity'], document['id']);
                return ProductListElement(d);
              }).toList(),
            );
        }
      },
    );
  }
  Widget elementBuild( ProductData data) {
    return ProductListElement(data);
  }
}

class ProductListElement extends StatelessWidget {
  final ProductData data;

  ProductListElement(this.data);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        data.name,
        style: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 20,
        ),
      ),
      trailing: Text(data.price.toString() + "€"),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ProductPage(data)));
      },
    );
  }
}
