

import 'package:flutter/material.dart';
import 'product_list.dart';
import 'package:fast_qr_reader_view/fast_qr_reader_view.dart';

class ProductListScreen extends StatelessWidget {
  final String title;

  ProductListScreen(this.title);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () => _settingModalBottomSheet(context),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => _scanBarcodeAndShowProduct(),
        icon: Icon(Icons.camera_alt),
        label: Text('Scan product'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: Center(
        child: ProductList(),
      ),
    );
  }
  Future _scanBarcodeAndShowProduct() async {
    //final File imageFile = getImageFile();


    //final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(imageFile);
      //String barcodeScanRes =  FlutterBarcodeScanner.scanBarcode("#FFFFFF", "Cancel", true) as String;
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  title: Center(
                    child: Text(
                      'Manage menu',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Add product'),
                    onTap: () => {}),
                ListTile(
                  leading: Icon(Icons.delete),
                  title: Text('Delete product'),
                  onTap: () => {},
                ),
              ],
            ),
          );
        });
  }
}
